package com.example.luk.pog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LUK on 2015-05-03.
 */
public class Country {
    String nazwa;
    boolean rozwiniente=false;
    List<City> miasta = new ArrayList<City>();

    public Country(String N) {
        nazwa = N;
    }

    void addCity(String N) {
        miasta.add(new City(N));
    }
    public void setRozwinienteTrue(){
        rozwiniente = true;
    }
    public void setRozwinienteFalse(){
        rozwiniente = false;
    }
}
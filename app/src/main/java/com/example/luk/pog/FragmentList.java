package com.example.luk.pog;

/**
 * Created by LUK on 2015-05-03.
 */
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class FragmentList extends Fragment {
    View view;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    List<Continent> kontynenty = new ArrayList<Continent>();
    List<String> test = new ArrayList<String>();
    ListView lista;
    String cos,tytul="";
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_main, container, false);
        if (test.isEmpty()) {
            stwoz();

            for (int i = 0; i < kontynenty.size(); i++) {
                test.add(kontynenty.get(i).nazwa);
            }
        }
        lista = (ListView) view.findViewById(R.id.miLista);
        lista.setAdapter(new TestAdapter(test));


        lista.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(android.widget.AdapterView<?> parent, View view, int position, long id) {
                cos = test.get(position);
                test.clear();
                addKontynenty(cos);
                lista.setAdapter(new TestAdapter(test));
            }
        });
        return view;
    }
    private void zmiana(){
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            FragmentNet2 fragment = (FragmentNet2) getFragmentManager().findFragmentById(R.id.fragment1);
            fragment.setTytul(cos);
        }
        else{

            getFragmentManager().beginTransaction()
                    .replace(R.id.container, new FragmentNet(cos)).addToBackStack(null)
                    .commit();
        }
    }
    class TestAdapter extends BaseAdapter {
        private List<String> ele;

        public TestAdapter(List<String> test) {
            ele = test;
        }

        @Override
        public int getCount() {
            return ele.size();
        }

        @Override
        public String getItem(int position) {
            return ele.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView result;
            if (convertView == null) {
                result = new TextView(parent.getContext());
                result.setText(ele.get(position));

            } else {
                result = (TextView) convertView;
                result.setText(ele.get(position));
            }
            //  result.setTextSize(25);

            return result;

        }

    }

    private void addMiasta(int i,int j,String cos){
        for (int k = 0; k < kontynenty.get(i).panstwa.get(j).miasta.size(); k++) {
            test.add("\t\t"+kontynenty.get(i).panstwa.get(j).miasta.get(k).nazwa);
            if (cos.equals("\t\t"+kontynenty.get(i).panstwa.get(j).miasta.get(k).nazwa)) {
                zmiana();
            }
        }
    }
    private void addPanstwa(int i,String cos){
        for (int j = 0; j < kontynenty.get(i).panstwa.size(); j++) {
            test.add("\t"+kontynenty.get(i).panstwa.get(j).nazwa);

            if (kontynenty.get(i).panstwa.get(j).rozwiniente) {
                if (cos.equals("\t"+kontynenty.get(i).panstwa.get(j).nazwa)) {
                    kontynenty.get(i).panstwa.get(j).setRozwinienteFalse();
                    continue;
                }
                else {
                    addMiasta(i,j,cos);
                }
            }
            else{
                if (cos.contains(kontynenty.get(i).panstwa.get(j).nazwa)) {
                    kontynenty.get(i).panstwa.get(j).setRozwinienteTrue();
                    addMiasta(i,j,cos);
                }
            }
        }
    }
    private void addKontynenty(String cos){
        for (int i = 0; i < kontynenty.size(); i++) {
            test.add(kontynenty.get(i).nazwa);
            if (kontynenty.get(i).rozwiniente) {
                if (kontynenty.get(i).nazwa==cos) {
                    kontynenty.get(i).setRozwinienteFalse();
                    continue;
                }
                else {
                    addPanstwa(i, cos);
                }
            }
            else {
                if (kontynenty.get(i).nazwa==cos) {
                    kontynenty.get(i).setRozwinienteTrue();
                    addPanstwa(i,cos);
                }
            }
        }
    }
    private void stwoz(){
        kontynenty.add(new Continent("Europa"));
        kontynenty.add(new Continent("Ameryka Południowa"));
        kontynenty.add(new Continent("Ameryka Północna"));

        kontynenty.get(0).addCountry("Polska");
        kontynenty.get(0).addCountry("Francja");
        kontynenty.get(0).addCountry("Niemcy");
        kontynenty.get(0).addCountry("Hiszpania");

        kontynenty.get(1).addCountry("Brazylia");
        kontynenty.get(1).addCountry("Peru");
        kontynenty.get(1).addCountry("Argentyna");
        kontynenty.get(1).addCountry("Boliwia");

        kontynenty.get(2).addCountry("USA");
        kontynenty.get(2).addCountry("Kanada");
        kontynenty.get(2).addCountry("Meksyk");

        kontynenty.get(0).panstwa.get(0).addCity("Zakopane");
        kontynenty.get(0).panstwa.get(0).addCity("Warszawa");
        kontynenty.get(0).panstwa.get(0).addCity("Bydgoszcz");
        kontynenty.get(0).panstwa.get(0).addCity("Sieradz");

        kontynenty.get(0).panstwa.get(1).addCity("Paryż");
        kontynenty.get(0).panstwa.get(1).addCity("Marsylia");
        kontynenty.get(0).panstwa.get(1).addCity("Lyon");
        kontynenty.get(0).panstwa.get(1).addCity("Niort");

        kontynenty.get(0).panstwa.get(2).addCity("Berlin");
        kontynenty.get(0).panstwa.get(2).addCity("Hamburg");
        kontynenty.get(0).panstwa.get(2).addCity("Brema");
        kontynenty.get(0).panstwa.get(2).addCity("Kolonia");

        kontynenty.get(0).panstwa.get(3).addCity("Madryt");
        kontynenty.get(0).panstwa.get(3).addCity("Kordoba");
        kontynenty.get(0).panstwa.get(3).addCity("Malaga");
        kontynenty.get(0).panstwa.get(3).addCity("Saragossa");

        kontynenty.get(1).panstwa.get(0).addCity("Salvador");
        kontynenty.get(1).panstwa.get(0).addCity("Fortaleza");
        kontynenty.get(1).panstwa.get(0).addCity("Manaus");
        kontynenty.get(1).panstwa.get(0).addCity("Rio de Janeiro");

        kontynenty.get(1).panstwa.get(1).addCity("Arequipa");
        kontynenty.get(1).panstwa.get(1).addCity("Cuzco");
        kontynenty.get(1).panstwa.get(1).addCity("Lima");
        kontynenty.get(1).panstwa.get(1).addCity("Trujillo");

        kontynenty.get(1).panstwa.get(2).addCity("Rosario");
        kontynenty.get(1).panstwa.get(2).addCity("Buenos Aires");
        kontynenty.get(1).panstwa.get(2).addCity("Quilmes");
        kontynenty.get(1).panstwa.get(2).addCity("La Plata");

        kontynenty.get(1).panstwa.get(3).addCity("Sucre");
        kontynenty.get(1).panstwa.get(3).addCity("Tarija");
        kontynenty.get(1).panstwa.get(3).addCity("Santa Cruz");
        kontynenty.get(1).panstwa.get(3).addCity("Sacaba");

        kontynenty.get(2).panstwa.get(0).addCity("Nowy Jork");
        kontynenty.get(2).panstwa.get(0).addCity("Waszyngton");
        kontynenty.get(2).panstwa.get(0).addCity("Miami");
        kontynenty.get(2).panstwa.get(0).addCity("Las Vegas");

        kontynenty.get(2).panstwa.get(1).addCity("Edmonton");
        kontynenty.get(2).panstwa.get(1).addCity("Calgary");
        kontynenty.get(2).panstwa.get(1).addCity("Ottawa");
        kontynenty.get(2).panstwa.get(1).addCity("Montreal");

        kontynenty.get(2).panstwa.get(2).addCity("Maksyk");
        kontynenty.get(2).panstwa.get(2).addCity("Monterrey");
        kontynenty.get(2).panstwa.get(2).addCity("Marida");
        kontynenty.get(2).panstwa.get(2).addCity("Merida");
    }

}
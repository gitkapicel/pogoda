package com.example.luk.pog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LUK on 2015-05-03.
 */
public class Continent {
    String nazwa;
    boolean rozwiniente=false;
    List<Country> panstwa;

    public Continent(String N) {
        nazwa = N;
        panstwa = new ArrayList<Country>();
    }

    void addCountry(String N) {
        panstwa.add(new Country(N));
    }
    public void setRozwinienteTrue(){
        rozwiniente = true;
    }
    public void setRozwinienteFalse(){
        rozwiniente = false;
    }

}

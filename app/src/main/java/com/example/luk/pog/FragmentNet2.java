package com.example.luk.pog;


import android.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;

/**
 * Created by LUK on 2015-05-11.
 */
public class FragmentNet2  extends Fragment {
    View view;
    ImageView image;
    private static TextView textview,textview2,textview3,textview4,textviewTytul,textviewDisconect;
    String url, tytul, strona2;

    public void setTytul(String q){
        tytul = q;
        boolean isNetAvailable = isNetworkAvailable();
        url = "http://api.openweathermap.org/data/2.5/weather?q="+ tytul.substring(2);

        textviewDisconect = (TextView) view.findViewById(R.id.brakNeta);
        textview = (TextView) view.findViewById(R.id.temp);
        textview2 = (TextView) view.findViewById(R.id.spead);
        textview3 = (TextView) view.findViewById(R.id.pressure);
        textview4 = (TextView) view.findViewById(R.id.clouds);
        textviewTytul = (TextView) view.findViewById(R.id.tytul);
        image =(ImageView) view.findViewById(R.id.imageView1);


        if (isNetAvailable) {
            textviewTytul.setText(tytul);
            textviewDisconect.setText("");
            JSONObject strona = null;
            MyRun myRun = new MyRun(url);
            Thread watek = new Thread(myRun);
            watek.start();
            try {
                watek.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            strona2 = myRun.get();
            try {
                InternalStorage.writeObject(this.getActivity(), "jstring", strona2);
            } catch (IOException e) {
                e.printStackTrace();
            }
            parseJson(strona2);
        }
        else{
            textviewDisconect.setText("Brak dostępu do Internetu. \nOstatnie pobrane dane");
            try {
                String jstring = (String) InternalStorage.readObject(this.getActivity(), "jstring");
                parseJson(jstring);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pogodanet, container, false);
        return view;
    }



    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }


    public void parseJson(String cos){
        if (cos != "" && cos != "qwe") {
            JSONObject strona;
            try {

                strona = new JSONObject(cos);
                JSONArray arr = strona.getJSONArray("weather");

                String pageName = arr.getJSONObject(0).getString("main");
                String pageName2 = arr.getJSONObject(0).getString("description");
                String pageName3 = strona.getJSONObject("main").getString("temp");
                String pageName4 = strona.getJSONObject("main").getString("pressure");
                String pageName5 = strona.getJSONObject("wind").getString("speed");
                String pageName6 = arr.getJSONObject(0).getString("icon");

                changeIcon("im" + pageName6);

                float amount = Float.parseFloat(pageName3);
                float cel = amount - 273.16f;
                String result = String.format("%.1f", cel);
                String tic = String.valueOf(result);

                textview.setText("Weather:" + pageName + "--" + pageName2);
                textview2.setText("Temp: " + tic);
                textview3.setText("Presure: " + pageName4);
                textview4.setText("Wind: " + pageName5);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            textview.setText("Wystąpił błąd. \nSpróbuj ponownie");
            textview2.setText("");
            textview3.setText("");
            textview4.setText("");
            changeIcon("im00");
        }


    }

    public void changeIcon(String q){
        if (q.equals("im01d"))
            image.setImageResource(R.drawable.im01d);
        if (q.equals("im01n"))
            image.setImageResource(R.drawable.im01n);
        if (q.equals("im02d"))
            image.setImageResource(R.drawable.im02d);
        if (q.equals("im02n"))
            image.setImageResource(R.drawable.im02n);
        if (q.equals("im03d"))
            image.setImageResource(R.drawable.im03d);
        if (q.equals("im03n"))
            image.setImageResource(R.drawable.im03n);
        if (q.equals("im04d"))
            image.setImageResource(R.drawable.im04d);
        if (q.equals("im04n"))
            image.setImageResource(R.drawable.im04n);
        if (q.equals("im09d"))
            image.setImageResource(R.drawable.im09d);
        if (q.equals("im09n"))
            image.setImageResource(R.drawable.im09n);
        if (q.equals("im10d"))
            image.setImageResource(R.drawable.im10d);
        if (q.equals("im10n"))
            image.setImageResource(R.drawable.im10n);
        if (q.equals("im11d"))
            image.setImageResource(R.drawable.im11d);
        if (q.equals("im11n"))
            image.setImageResource(R.drawable.im11n);
        if (q.equals("im13d"))
            image.setImageResource(R.drawable.im13d);
        if (q.equals("im13n"))
            image.setImageResource(R.drawable.im13n);
        if (q.equals("im50d"))
            image.setImageResource(R.drawable.im50d);
        if (q.equals("im50n"))
            image.setImageResource(R.drawable.im50n);
    }


}
